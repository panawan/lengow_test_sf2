<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}", name="hometestpage")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }
    
    /**
     * @Route("/url", name="url")
     * @Template()
     */
    public function orderAction()
    {
        //return $this->redirect('http://test.lengow.io/orders-test.xml');
        $this->container = new ContainerBuilder();
        //return $this->redirect('http://test.lengow.io/orders-test.xml');
        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('parameters.yml');
        
        return $this->redirect($this->container->getParameter('url_orders'));
    }
}
