<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use TestBundle\Model\ParserXML;

class ParseXmlController extends Controller
{
    /**
     * @Route("/order", name="url_order")
     * @Template()
     */
    public function xmlAction()
    {
        $loadXmlPage = new ParserXML();
        $dataXml = $loadXmlPage->loadFileXml();
        
        foreach($dataXml->children() as $orders){
           $test = $orders->{"cdiscount"}.'<br>';	
        }
        
        echo '<pre>';
        print_r($dataXml);
        echo '</pre>';
        
        return $this->render('TestBundle:Default:dataxml.html.twig',array('xml'=> $dataXml, 'test' => $test));
    }
}
