<?php
namespace TestBundle\Model;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

/**
 * ParserXML class
 * 
 * @author Fred
 */

class ParserXML
{

    private $url_orders;
    private $container;

    public function loadFileXml()
    {
        $this->container = new ContainerBuilder();
        $loader = new YamlFileLoader($this->container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('parameters.yml');
        $this->url_orders = $this->container->getParameter('url_orders');
        
        if ($this->url_orders) {
            try{
                $xml = simplexml_load_file($this->url_orders);
                foreach($xml->children() as $orders){
                        echo $orders->{"cdiscount"}.'<br>';	
                }
                return $xml;
            } catch (Exception $e) {
                echo 'Echec lors de l\'ouverture de '.$this->url_orders,  $e->getMessage(), "\n";
                exit();
            }
        }
    }
    
}